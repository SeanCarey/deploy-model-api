from fastai.text import load_learner

# load the trained model from .pkl file
learn = load_learner("", file="export.pkl")


# take text from request json save as a string
def extract_text_from_request(request_json):
    request_text = request_json["text"]
    return request_text


# send the string to the model to be classified return the response from the model
def predict_text_political_class(request_text):
    pred = learn.predict(request_text)
    return pred


# return the prediction as a dictionary
# extract correct portion of the prediction based on the predicted class.
def prediction_to_dict(prediction):
    # if response is liberal
    if int(prediction[0]) == 1:
        political_class = 'Liberal'
        percent_in_class = round(float(prediction[2][1]), 2) * 100

        response_dict = {'predicted_class': political_class,
                         'percent_in_class': percent_in_class}
        return response_dict

    # if response is conservative
    else:
        political_class = 'Conservative'
        percent_in_class = round(float(prediction[2][0]), 2) * 100
        response_dict = {'predicted_class': political_class,
                         'percent_in_class': percent_in_class}
        return response_dict
