#! /bin/bash

curl -X POST -H "Content-Type: application/json" -d @to_predict_json.json https://seancarey-deploy-model-api.138.197.50.110.nip.io/api
curl -X POST -H "Content-Type: application/json" -d @to_predict_bad_dict.json https://seancarey-deploy-model-api.138.197.50.110.nip.io/api
curl -X POST -H "Content-Type: application/json" -d @to_predict_not_str.json https://seancarey-deploy-model-api.138.197.50.110.nip.io/api

#