import requests

url = 'https://seancarey-deploy-model-api.138.197.50.110.nip.io/api'

payload = {"text": "The world's richest 1% have over twice the wealth of 6.9 billion people. \
The planet will not be secure or peaceful when so few have so much and so many have so little.."}


res = requests.post(url, json=payload)

data = res.json()

print(data)
