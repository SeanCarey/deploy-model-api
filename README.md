
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">

  </a>

  <h3 align="center">Flask API</h3>

  <p align="center">
    Text Classification Model in Production
    <br />
  </p>
</p>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Practices and Technologies](#practices-and-technologies)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
* [Installation and Testing](#installation-and-testing)
* [Test the API](#test-the-api)



<!-- ABOUT THE PROJECT -->
## About The Project
Flask is an easy to use Python based Web Micro-Framework. Using flask and GitLab, this project demonstrates using DevOps practices to serve a machine learning model as an API.

# Built With

### Practices and Technologies

* [Flask](https://palletsprojects.com/p/flask/)
* [CICD](https://medium.com/@nirespire/what-is-cicd-concepts-in-continuous-integration-and-deployment-4fe3f6625007)
* [Unit Testing with PyTest](https://docs.pytest.org/en/latest/)
* [Docker](https://www.nginx.com/)
* [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)


# Installation
<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* Python (3.7+)
* Docker
* Docker-Compose

### Installation and Testing
Install and Test this application

**On Ubuntu**
1. [Install Docker](https://docs.docker.com/installation/ubuntulinux/)
```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
2. [Install Docker Compose](https://docs.docker.com/compose/install/)
```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
3. Clone the Git Repository
```sh
git clone https://gitlab.com/SeanCarey/deploy-model-api.git
cd deploy-model-api
```
4. Install PyTest  
```sh
pip install pytest
```
5. Run Unit Tests
```sh
pytest -p no:warnings
```

6. Build and Run Containers
```sh
docker build -t modelapi:latest .  
docker run -p 5000:5000 modelapi:latest 
```

**On Mac**
1. [Install Docker](https://docs.docker.com/docker-for-mac/install/)
2. [Install Docker Compose](https://docs.docker.com/docker-for-mac/install/)

3. Clone the Git Repository
```sh
git clone https://gitlab.com/SeanCarey/deploy-model-api.git
cd deploy-model-api
```
4. Install PyTest  
```sh
pip install pytest
```
5. Run Unit Tests
```sh
pytest -p no:warnings
```

6. Build and Run Containers
```sh
docker build -t modelapi:latest .  
docker run -p 5000:5000 modelapi:latest 
```

### Test the API

[Install Curl on Mac](https://www.code2bits.com/how-to-install-curl-on-macos-using-homebrew/)

[Install Curl on Ubuntu](https://linuxize.com/post/how-to-install-and-use-curl-on-ubuntu-18-04/)

The curl script uses the a pre-made JSONs included in the ```manual_test_scripts``` folder. 

If you want to send a custom request edit ```to_predict_json.json``` before sending the curl request.

```sh
cd manual_test_scripts
curl -X POST -H "Content-Type: application/json" -d  @to_predict_json.json http://127.0.0.1:5000/api
```
